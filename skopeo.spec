%global with_debug 1

%if 0%{?with_debug}
%global _find_debuginfo_dwz_opts %{nil}
%global _dwz_low_mem_die_limit 0
%else
%global debug_package %{nil}
%endif

%global gomodulesmode GO111MODULE=on

# No btrfs on RHEL
%if %{defined fedora}
%define build_with_btrfs 1
%endif

# Only used in official koji builds
# Copr builds set a separate epoch for all environments
%if %{defined fedora}
%define conditional_epoch 1
%else
%define conditional_epoch 2
%endif

Name: skopeo
%if %{defined copr_username}
Epoch: 102
%else
Epoch: %{conditional_epoch}
%endif
# DO NOT TOUCH the Version string!
# The TRUE source of this specfile is:
# https://github.com/containers/skopeo/blob/main/rpm/skopeo.spec
# If that's what you're reading, Version must be 0, and will be updated by Packit for
# copr and koji builds.
# If you're reading this on dist-git, the version is automatically filled in by Packit.
Version: 1.18.0
# The `AND` needs to be uppercase in the License for SPDX compatibility
License: Apache-2.0 AND BSD-2-Clause AND BSD-3-Clause AND ISC AND MIT AND MPL-2.0
Release: 1%{?dist}
%if %{defined golang_arches_future}
ExclusiveArch: %{golang_arches_future}
%else
ExclusiveArch: aarch64 ppc64le s390x x86_64
%endif
Summary: Inspect container images and repositories on registries
URL: https://github.com/containers/%{name}
# Tarball fetched from upstream
Source0: %{url}/archive/v%{version}.tar.gz
BuildRequires: %{_bindir}/go-md2man
%if %{defined build_with_btrfs}
BuildRequires: btrfs-progs-devel
%endif
BuildRequires: git-core
BuildRequires: golang
%if !%{defined gobuild}
BuildRequires: go-rpm-macros
%endif
BuildRequires: gpgme-devel
BuildRequires: libassuan-devel
BuildRequires: ostree-devel
BuildRequires: glib2-devel
BuildRequires: make
BuildRequires: shadow-utils-subid-devel
Requires: containers-common >= 4:1-21

%description
Command line utility to inspect images and repositories directly on Docker
registries without the need to pull them

%package tests
Summary: Tests for %{name}

Requires: %{name} = %{epoch}:%{version}-%{release}
Requires: bats
Requires: gnupg
Requires: jq
Requires: golang
Requires: podman
Requires: crun
Requires: httpd-tools
Requires: openssl
%if %{defined fedora}
Requires: fakeroot
Requires: squashfs-tools
%endif

%description tests
%{summary}

This package contains system tests for %{name}. Only intended for distro gating
tests. End user / customer usage not supported.

%prep
%autosetup -Sgit %{name}-%{version}
# The %%install stage should not rebuild anything but only install what's
# built in the %%build stage. So, remove any dependency on build targets.
sed -i 's/^install-binary: bin\/%{name}.*/install-binary:/' Makefile
sed -i 's/^completions: bin\/%{name}.*/completions:/' Makefile
sed -i 's/^install-docs: docs.*/install-docs:/' Makefile

%build
%set_build_flags
export CGO_CFLAGS=$CFLAGS

# These extra flags present in $CFLAGS have been skipped for now as they break the build
CGO_CFLAGS=$(echo $CGO_CFLAGS | sed 's/-flto=auto//g')
CGO_CFLAGS=$(echo $CGO_CFLAGS | sed 's/-Wp,D_GLIBCXX_ASSERTIONS//g')
CGO_CFLAGS=$(echo $CGO_CFLAGS | sed 's/-specs=\/usr\/lib\/rpm\/redhat\/redhat-annobin-cc1//g')

%ifarch x86_64
export CGO_CFLAGS="$CGO_CFLAGS -m64 -mtune=generic -fcf-protection=full"
%endif

BASEBUILDTAGS="$(hack/libsubid_tag.sh)"
%if %{defined build_with_btrfs}
export BUILDTAGS="$BASEBUILDTAGS $(hack/btrfs_tag.sh) $(hack/btrfs_installed_tag.sh)"
%else
export BUILDTAGS="$BASEBUILDTAGS btrfs_noversion exclude_graphdriver_btrfs libtrust_openssl"
%endif

# unset LDFLAGS earlier set from set_build_flags
LDFLAGS=''

%gobuild -o bin/%{name} ./cmd/%{name}
%{__make} docs

%install
make \
    DESTDIR=%{buildroot} \
    PREFIX=%{_prefix} \
    install-binary install-docs install-completions

# system tests
install -d -p %{buildroot}/%{_datadir}/%{name}/test/system
cp -pav systemtest/* %{buildroot}/%{_datadir}/%{name}/test/system/

#define license tag if not already defined
%{!?_licensedir:%global license %doc}

# Placeholder check to silence rpmlint
%check

%files
%license LICENSE
%doc README.md
%{_bindir}/%{name}
%{_mandir}/man1/%{name}*
%dir %{_datadir}/bash-completion
%dir %{_datadir}/bash-completion/completions
%{_datadir}/bash-completion/completions/%{name}
%dir %{_datadir}/fish/vendor_completions.d
%{_datadir}/fish/vendor_completions.d/%{name}.fish
%dir %{_datadir}/zsh/site-functions
%{_datadir}/zsh/site-functions/_%{name}

%files tests
%license LICENSE vendor/modules.txt
%{_datadir}/%{name}/test

%changelog
* Thu Feb 13 2025 Jindrich Novy <jnovy@redhat.com> - 1:1.18.0-1
- update to https://github.com/containers/skopeo/releases/tag/v1.18.0
- Related: RHEL-58990

* Thu Jan 23 2025 Jindrich Novy <jnovy@redhat.com> - 1:1.17.0-3
- fix 'Skopeo system tests pulling amd64 image on aarch64'
- Resolves: RHEL-76027

* Tue Dec 10 2024 Jindrich Novy <jnovy@redhat.com> - 1:1.17.0-2
- remove squashfs-tools from RHEL10 build
- Related: RHEL-68938

* Mon Nov 25 2024 Jindrich Novy <jnovy@redhat.com> - 1:1.17.0-1
- update to https://github.com/containers/skopeo/releases/tag/v1.17.0
- Related: RHEL-58990

* Tue Oct 29 2024 Troy Dawson <tdawson@redhat.com> - 2:1.16.1-2
- Bump release for October 2024 mass rebuild:
  Resolves: RHEL-64018

* Mon Aug 26 2024 Jindrich Novy <jnovy@redhat.com> - 1:1.16.1-1
- update to https://github.com/containers/skopeo/releases/tag/v1.16.1
- Related: RHEL-34195

* Tue Jul 30 2024 Jindrich Novy <jnovy@redhat.com> - 1:1.16.0-2
- Don't require fakeroot for tests
- Resolves: RHEL-27608

* Mon Jul 29 2024 Jindrich Novy <jnovy@redhat.com> - 1:1.16.0-1
- Update to version 1.16.0

* Fri Jul 12 2024 Jindrich Novy <jnovy@redhat.com> - 1:1.15.2-1
- Update to version 1.15.2

* Mon Jun 24 2024 Troy Dawson <tdawson@redhat.com> - 2:1.15.1-2
- Bump release for June 2024 mass rebuild

* Thu May 16 2024 Jindrich Novy <jnovy@redhat.com> - 2:1.15.1-1
- update to https://github.com/containers/skopeo/releases/tag/v1.15.1
- Related: RHEL-34195

* Thu Mar 28 2024 Jindrich Novy <jnovy@redhat.com> - 2:1.15.0-3
- BR: go-rpm-macros
- Related: RHEL-30637

* Thu Mar 28 2024 Jindrich Novy <jnovy@redhat.com> - 2:1.15.0-2
- remove Fedora hack
- Related: RHEL-30637

* Thu Mar 28 2024 Jindrich Novy <jnovy@redhat.com> - 2:1.15.0-2.14.2
- Sync with RHEL9
- Resolves: RHEL-30637
